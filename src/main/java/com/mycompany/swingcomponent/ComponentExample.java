/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swingcomponent;

import java.awt.Color;
import java.awt.Graphics;
import javax.lang.model.SourceVersion;
import javax.swing.JComponent;
import javax.swing.JFrame;

/**
 *
 * @author ACER
 */
class MyComponentExample extends JComponent{
    @Override
    public void paint(Graphics g) {  
        g.setColor(Color.green);  
        g.fillRect(30, 30, 100, 100);  
      }
}
public class ComponentExample{
    public static void main(String[] args) {
        MyComponentExample com = new MyComponentExample();
        JFrame.setDefaultLookAndFeelDecorated(true);  
        JFrame frame = new JFrame("JComponent Example");  
        frame.setSize(300,200);  
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(com);  
        frame.setVisible(true);
    }  
}
