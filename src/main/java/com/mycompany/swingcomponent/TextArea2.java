/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swingcomponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;

/**
 *
 * @author ACER
 */
public class TextArea2 implements ActionListener {

    JLabel lb1, lb2;
    JTextArea area;
    JButton b;

    TextArea2() {
        JFrame f= new JFrame();  
        lb1=new JLabel();  
        lb1.setBounds(50,25,100,30);  
        lb2=new JLabel();  
        lb2.setBounds(160,25,100,30);  
        area=new JTextArea();  
        area.setBounds(20,75,250,200);  
        b=new JButton("Count Words");  
        b.setBounds(100,300,120,30);  
        b.addActionListener(this);  
        f.add(lb1);
        f.add(lb2);
        f.add(area);
        f.add(b);  
        f.setSize(450,450);  
        f.setLayout(null);  
        f.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        String text=area.getText();  
        String words[]=text.split("\\s");  
        lb1.setText("Words: "+words.length);  
        lb2.setText("Characters: "+text.length());
    }
    public static void main(String[] args) {
        new TextArea2();
    }
}
