/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swingcomponent;

import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author ACER
 */
public class LabelEx {
    public static void main(String[] args) {
        JFrame f = new JFrame("Label");
        JLabel lb1,lb2;
        lb1 = new JLabel("First Label.");
        lb1.setBounds(50, 50, 100, 30);
        lb2 = new JLabel("Second Label");
        lb2.setBounds(50, 100, 100, 30);
        f.add(lb1);
        f.add(lb2);
        f.setSize(300, 300);
        f.setLayout(null);
        f.setVisible(true);
    }
}
