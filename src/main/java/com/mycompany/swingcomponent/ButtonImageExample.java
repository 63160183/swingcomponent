/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swingcomponent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author ACER
 */
public class ButtonImageExample {
    ButtonImageExample(){
        JFrame f = new JFrame("Button Image Example");
        JButton b = new JButton(new ImageIcon("C:\\Users\\ACER\\OneDrive\\Pictures\\Screenshots\\register-clipart.png"));
        b.setBounds(100,100,100, 40);    
        f.add(b);    
        f.setSize(300,400);    
        f.setLayout(null);    
        f.setVisible(true);    
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    public static void main(String[] args) {
        new ButtonImageExample();
    }
}
