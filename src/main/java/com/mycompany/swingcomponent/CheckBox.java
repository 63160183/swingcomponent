/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swingcomponent;

import javax.swing.JCheckBox;
import javax.swing.JFrame;

/**
 *
 * @author ACER
 */
public class CheckBox {
    CheckBox(){
        JFrame f = new JFrame("Checkbox Example");
        JCheckBox checkBox1 = new JCheckBox("C++");
        checkBox1.setBounds(100, 100, 50, 50);
        JCheckBox checkBox2 = new JCheckBox("Java",true);
        checkBox2.setBounds(100,150, 100,50);  
        f.add(checkBox1);  
        f.add(checkBox2);  
        f.setSize(400,400);  
        f.setLayout(null);  
        f.setVisible(true); 
    }
    public static void main(String[] args) {
        new CheckBox();
    }
}
