/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.swingcomponent;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.JFrame;

/**
 *
 * @author ACER
 */
public class DisplayingImage extends Canvas{
    @Override
    public void paint(Graphics g) {  
        Toolkit t=Toolkit.getDefaultToolkit();  
        Image i=t.getImage("D:\\200.gif");  
        g.drawImage(i, 120,100,this);         
    }
    public static void main(String[] args) {
        DisplayingImage DI = new DisplayingImage();
        JFrame f=new JFrame();  
        f.add(DI);  
        f.setSize(500,500);  
        f.setVisible(true);
    }
}
